require "yaml"

require_relative "hash"
require_relative "unotifier/version"
require_relative "unotifier/system_notifier"
require_relative "unotifier/user_notifier"
require_relative "exceptions"
require_relative "provider"
require_relative "configuration"
require_relative "settings"

module UNotifier
  class << self
    attr_writer :configuration
  end

  def self.configuration
    @configuration ||= Configuration.new
  end

  def self.configure
    yield(configuration)
  end

  def self.notifications_config
    @notifications_config ||= if configuration.notifications_path.is_a?(Array)
                                configuration.notifications_path.each_with_object({}) do |path, config|
                                  config.deep_merge!(YAML.load_file(path))
                                end
                              else
                                YAML.load_file(configuration.notifications_path)
                              end
  rescue Errno::ENOENT
    raise NotificationsConfigNotFoundError, configuration.notifications_path
  end

  def self.reload!
    @configuration = nil
    @notifications_config = nil
  end

  def self.load_notification(key)
    notifications_config.dig(*key.split("."))
  end

  def self.load_notification!(key)
    notification = load_notification(key)
    raise NotificationNotFoundError, key unless notification

    notification
  end

  def self.locale_key_for(key, params = {})
    config = load_notification!(key)["user"]

    if config["locale_options"].is_a?(Array)
      raise EmptyLocaleKeyError, key unless params[:locale_key]

      raise UnknownLocaleKeyError.new(
        key, params[:locale_key], config["locale_options"]
      ) unless config["locale_options"].include?(params[:locale_key])

      "notifications.#{key}.#{params[:locale_key]}"
    else
      "notifications.#{key}"
    end
  end

  def self.notify(key, recepients, params = {})
    targets = recepients.is_a?(Enumerable) ? recepients : [recepients]

    targets.each do |target|
      notify_target(key, target, params)
    end
  end

  def self.notify_target(key, target, params = {})
    locale_key = locale_key_for(key, params)
    urgency = urgency_for(key, params)

    notification = configuration.resource_class.new(
      key: key,
      target: target,
      link: params[:link],
      autohide_delay: params[:autohide_delay],
      urgency: urgency,
      title: configuration.localization_provider.t(
        "#{locale_key}.title", params.merge(locale: target.locale)
      ),
      body: configuration.localization_provider.t(
        "#{locale_key}.body", params.merge(locale: target.locale, default: "")
      )
    )

    notification.save!

    UserNotifier.call(notification, params)
    SystemNotifier.call(notification, params[:system])
  end

  def self.notify_onsite(key, target, params)
    notify(key, target, params.merge(onsite_only: true))
  end

  def self.notify_external(key, target, params)
    notify(key, target, params.merge(external_only: true))
  end

  def self.notification_settings_for(target)
    user_settings = target.notification_settings
    Settings
      .grouped_by_urgency_keys_from(notifications_config)
      .each_with_object({}) do |(level, grouped_keys), obj|
        obj[level] =
          grouped_keys.each_with_object({}) do |(urgency, keys), settings|
            settings[urgency] = keys.each_with_object({}) do |(key, _), out|
              out[key] =
                user_settings.is_a?(Hash) && user_settings.dig(level, key) ||
                Settings::DEFAULT_URGENCY
            end
          end
      end
      .merge("options" => user_settings["options"])
  end

  def self.hide_sensitive_for?(target)
    !!target.notification_settings.dig("options", "hide_sensitive")
  end

  def self.urgency_for(key, params = {})
    config = load_notification!(key)["user"]

    if config["target"].is_a?(Hash)
      urgency = config.dig("target", params[:target], "urgency")
      raise UnknownTargetError.new(key, params[:target]) unless urgency

      urgency
    else
      config["urgency"]
    end
  end
end
