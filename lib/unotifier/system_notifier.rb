module UNotifier
  module SystemNotifier
    def self.call(notification, params = {})
      key = notification.key
      config = UNotifier.load_notification!(key)
      return if config["system"].nil?

      # Raise only when we're sure that config has "system" block
      raise AttributeMissingError.new(key, "system") unless params

      if config["system"]["urgency"] == "regular"
        # Don't notify if user disabled the notification in settings
        user_settings =
          notification.target.notification_settings.is_a?(Hash) &&
          notification.target.notification_settings.dig("system", notification.key)
        return if user_settings == "off"
      end

      data = config["system"]["keys"].each_with_object({}) do |(head, attributes), obj|
        head = head.to_sym
        attributes = attributes.map(&:to_sym)
        raise AttributeMissingError.new(key, head) unless params.key?(head)

        attributes.each { |a| raise AttributeMissingError.new(key, a) unless params[head].keys.include?(a) }

        obj[head] = params[head].slice(*attributes)
      end

      data.merge!(key: notification.key)

      UNotifier.configuration.system_providers.each do |provider|
        provider.notify notification, data
      end
    end
  end
end
