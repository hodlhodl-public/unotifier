module UNotifier
  module Provider
    module User
      class ActionCable < ProviderBase
        attr_reader :server
        attr_accessor :autohide_delay, :channel_name

        def initialize(server, channel_name: nil, autohide_delay: 30, notification_conditions: [])
          super(notification_conditions: notification_conditions)
          @server = server
          @channel_name = channel_name
          @autohide_delay = autohide_delay
        end

        def notify(notification)
          return unless can_notify?(notification)

          server.broadcast channel_name.call(notification), serialize_notification(notification)
        end

        def sensitive?
          false
        end

        def serialize_notification(notification)
          {
            id: notification.id,
            title: notification.title,
            body: notification.body,
            autohide_delay: notification.autohide_delay || @autohide_delay,
            user_login: notification.target.login,
            link: notification.link,
            urgency: notification.urgency,
          }
        end
      end
    end
  end
end
