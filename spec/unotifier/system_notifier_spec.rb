class SystemProvider
  def notify; end
end

RSpec.describe UNotifier::SystemNotifier do
  let(:notification_attributes) {
    {
      user: {
        login: "login",
        email: "email@email.email",
      },
      message: {
        content: "Message text",
      },
    }
  }
  let(:notification) { double("Notification")}
  let(:target) { double("Target") }
  let(:params) { notification_attributes }
  let(:system_provider) { SystemProvider.new }
  let(:key) { "first_category.n_immediate" }
  let(:notifications_path) { File.join(__dir__, "..", "fixtures", "notifications.yml") }

  before do
    UNotifier.reload!
    UNotifier.configure do |config|
      config.system_providers = [system_provider]
      config.notifications_path = notifications_path
    end

    allow(notification).to receive(:key).and_return(key)
    allow(notification).to receive(:target).and_return(target)
    allow(target).to receive(:notification_settings).and_return({})
  end

  subject { described_class.call(notification, params) }

  it "notifies with system providers" do
    expect { subject }.to notify_with([system_provider])
  end

  context "when user disabled the notification in settings" do
    before do
      allow(target).to receive(:notification_settings).and_return(
        "system" => {
          "first_category.n_system" => "off",
          "first_category.n_immediate" => "off",
        }
      )
    end

    context "when notification is regular" do
      let(:key) { "first_category.n_system" }

      it "doesn't send the notification" do
        expect { subject }.not_to notify_with([system_provider])
      end
    end

    context "when notification is immediate" do
      let(:key) { "first_category.n_immediate" }

      it "sends the notification anyway" do
        expect { subject }.to notify_with([system_provider])
      end
    end
  end

  context "when params contain attributes not defined in config" do
    let(:params) { notification_attributes.merge(os: { name: "Linux" }) }

    it "takes only defined by config attributes" do
      expect(system_provider).to(
        receive(:notify)
        .with(notification, notification_attributes.merge(key: key))
      )
      subject
    end
  end

  context "when provided attributes don't have required top-level key" do
    let(:params) { { message: { content: "123" } } }

    it "raises AttributeMissingError" do
      expect { subject }.to raise_error(UNotifier::AttributeMissingError, /user/)
    end
  end

  context "when one of provided attributes doesn't have required key" do
    let(:params) { { user: { login: "login" }, message: { content: "123"} } }

    it "raises AttributeMissingError" do
      expect { subject }.to raise_error(UNotifier::AttributeMissingError, /email/)
    end
  end
end
