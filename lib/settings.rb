module UNotifier
  class Settings
    DEFAULT_URGENCY = "external".freeze

    def self.customizable?(config)
      (
        config.key?("urgency") &&
          %w(regular optional).include?(config["urgency"])
      ) || (
        config.key?("target") &&
          config["target"].values.any? { |c| customizable?(c) }
      )
    end

    def self.filter_user_customizable(config)
      customizable = config.map do |key, subkeys|
        filtered =
          subkeys
          .select { |_, value| value.key?("user") }
          .map { |subkey, value| [subkey, value["user"]] }
          .select { |_, value| customizable?(value) }
          .each_with_object({}) do |(subkey, value), out|
            out[subkey] = value["urgency"] ||
                          value["target"]
                          .select { |_, subvalue| customizable?(subvalue) }
                          .map { |_, subvalue| subvalue["urgency"] }
                          .first
          end

        [key, filtered]
      end

      customizable.to_h.reject { |_, subkeys| subkeys.empty? }
    end

    def self.filter_system_customizable(config)
      customizable = config.map do |key, subkeys|
        filtered =
          subkeys
          .select { |_, value| value.key?("system") }
          .map { |subkey, value| [subkey, value["system"]] }
          .select { |_, value| value["urgency"] == "regular" }
          .each_with_object({}) do |(subkey, value), out|
            out[subkey] = value["urgency"]
          end

        [key, filtered]
      end

      customizable.to_h.reject { |_, subkeys| subkeys.empty? }
    end

    def self.keys_from(config)
      {
        "user" => flatten_keys(filter_user_customizable(config)),
        "system" => flatten_keys(filter_system_customizable(config)),
      }
    end

    def self.grouped_by_urgency_keys_from(config)
      keys_from(config).each_with_object({}) do |(channel, keys), obj|
        obj[channel] =
          keys.each_with_object({}) do |(key, urgency), out|
            out[urgency] ||= []
            out[urgency] << key
          end
      end
    end

    def self.flatten_keys(keys)
      keys.map do |key, subkeys|
        subkeys.map do |subkey, urgency|
          { "#{key}.#{subkey}" => urgency }
        end
      end.flatten.reduce({}, :merge)
    end
  end
end
