module UNotifier
  module UserNotifier
    def self.call(notification, params = {})
      config = UNotifier.load_notification!(notification.key)["user"]
      user_settings = _load_user_settings(notification)
      providers = _load_providers(notification, user_settings, params)

      # By default we assume that notification is sensitive
      is_sensitive = !config.has_key?("sensitive") || config["sensitive"]

      providers.each do |provider|
        is_private = is_sensitive &&
                     provider.sensitive? &&
                     UNotifier.hide_sensitive_for?(notification.target)

        if is_private
          notification.title = _translate(provider, notification, "title_private", params)
          notification.body = ""
        else
          notification.title = _translate(provider, notification, "title", params)
          notification.body = _translate(provider, notification, "body", params)
        end

        provider.notify(notification)
      end
    end

    def self._load_providers(notification, user_settings, params)
      providers = []

      if !params[:external_only] && _notify_onsite?(notification, user_settings)
        providers += UNotifier.configuration.site_providers
      end

      if !params[:onsite_only] && _notify_external?(notification, user_settings)
        providers += UNotifier.configuration.external_providers
      end

      providers
    end

    def self._load_user_settings(notification)
      (
        notification.target.notification_settings.is_a?(Hash) &&
        notification.target.notification_settings.dig("user", notification.key)
      ) ||
        Settings::DEFAULT_URGENCY
    end

    def self._notify_external?(notification, user_settings)
      case notification.urgency
      when "immediate"
        true
      when "regular", "optional"
        user_settings == "external" && !notification.target.online?
      when "onsite"
        false
      else
        false
      end
    end

    def self._notify_onsite?(notification, user_settings)
      return false if notification.urgency == "optional" && user_settings == "off"

      notification.target.online?
    end

    def self._translate(provider, notification, part, params)
      locale_key = UNotifier.locale_key_for(notification.key, params)
      locale_provider = UNotifier.configuration.localization_provider

      plain_key = "#{locale_key}.#{part}"
      provider_key = "#{locale_key}.#{provider.class.to_s.split("::").last}.#{part}"

      key =
        if locale_provider.exists?(provider_key)
          provider_key
        elsif locale_provider.exists?(plain_key)
          plain_key
        else
          "notifications.default.#{part}"
        end

      locale_provider.t(key, params.merge(locale: notification.target.locale))
    end
  end
end
