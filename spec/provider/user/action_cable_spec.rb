RSpec.describe UNotifier::Provider::User::ActionCable do
  let(:notification_attrs) {
    {
      id: 1,
      title: "title",
      body: "body",
      autohide_delay: "10",
      user_login: "login",
      link: "link",
      urgency: "regular",
    }
  }
  let(:target) { double("Target") }
  let(:notification) { double("Notification") }
  let(:server) { double("ActionCable Server") }
  let(:provider) {
    described_class.new(
      server,
      channel_name: -> (login) { "test_channel_#{login}" }
    )
  }

  before do
    allow(target).to receive(:login).and_return("login")
    allow(notification).to receive(:target).and_return(target)
    notification_attrs.each { |k, v| allow(notification).to receive(k).and_return(v) }
  end

  describe "#notify" do
    it "broadcasts notification" do
      expect(server).to receive(:broadcast).with(provider.channel_name.call(notification), kind_of(Hash)).once
      provider.notify(notification)
    end
  end

  describe "#serialize_notification" do
    it "returns hash with notification attributes" do
      expect(provider.serialize_notification(notification)).to include(notification_attrs)
    end
  end
end
