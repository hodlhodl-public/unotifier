RSpec.describe UNotifier::Provider::System::ActionCable do
  let(:notification_attrs) {
    {
      id: 1,
      title: "title",
      body: "body",
      autohide_delay: "10",
      user_login: "login",
      link: "link",
      urgency: "regular",
    }
  }
  let(:data) {
    {
      first_attribute: "first_value",
      second_attribute: "second_value",
    }
  }
  let(:notification) { double("Notification") }
  let(:target) { double("Target") }
  let(:server) { double("ActionCable Server") }
  let(:provider) {
    described_class.new(
      server,
      channel_name: -> (n) { "test_channel_#{n.target.login}" }
    )
  }

  before do
    allow(target).to receive(:login).and_return("login")
    allow(notification).to receive(:target).and_return(target)
    notification_attrs.each { |k, v| allow(notification).to receive(k).and_return(v) }
  end

  describe "#notify" do
    subject { provider.notify(notification, data) }

    it "broadcasts provided data" do
      expect(server).to receive(:broadcast).with(provider.channel_name.call(notification), data).once
      subject
    end
  end
end
