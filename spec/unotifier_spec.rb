require "i18n"

RSpec.describe UNotifier do
  it "has a version number" do
    expect(UNotifier::VERSION).not_to be nil
  end

  let(:notifications_path) { File.join(__dir__, "fixtures", "notifications.yml") }
  let(:notifications_fixture) { YAML.load_file(notifications_path) }
  let(:target) { double("Target") }
  let(:resource_class) { double("Resource Class") }

  before do
    I18n.available_locales << :en

    UNotifier.reload!
    UNotifier.configure do |config|
      config.notifications_path = notifications_path
      config.localization_provider = I18n
      config.resource_class = resource_class
    end
  end

  describe "#notifications_config" do
    context "when config file exists" do
      it "reads notifications hash from provided config file" do
        expect(UNotifier.notifications_config).to eq(notifications_fixture)
      end
    end

    context "when config file doesn't exist" do
      let(:notifications_path) { "nonexistent_file" }

      it "raises NotificationsConfigNotFoundError" do
        expect { UNotifier.notifications_config }
          .to raise_error(UNotifier::NotificationsConfigNotFoundError)
      end
    end
  end

  describe "#load_notification" do
    subject { UNotifier.load_notification(key) }

    context "when notification key exists" do
      let(:key) { "first_category.n_regular" }

      it { is_expected.to eq(
        notifications_fixture["first_category"]["n_regular"]
      ) }
    end

    context "when notification key doesn't exist" do
      let(:key) { "something_nonexistent" }

      it { is_expected.to be_nil }
    end
  end

  describe "#load_notification!" do
    context "when notification key doesn't exist" do
      it "raises NotificationNotFoundError" do
        expect { UNotifier.load_notification!("nonexistent") }
          .to raise_error(UNotifier::NotificationNotFoundError)
      end
    end
  end

  describe "#locale_key_for" do
    context "when notification key exists" do
      context "when config doesn't have locale_options" do
        it "returns locale_key composed from notification key name" do
          expect(described_class.locale_key_for("first_category.n_regular"))
            .to eq("notifications.first_category.n_regular")
        end
      end

      context "when config has locale_options" do
        context "when parameters have :locale_key attribute" do
          context "when :locale_key has matching option" do
            it "returns locale key for provided parameter" do
              expect(
                described_class.locale_key_for("second_category.n_multiple_locales",
                                        locale_key: "on_page")
              ).to eq("notifications.second_category.n_multiple_locales.on_page")
            end
          end

          context "when :locale_key doesn't have matching option" do
            it "raises UnknownLocaleKeyError" do
              expect {
                described_class.locale_key_for("second_category.n_multiple_locales",
                                        locale_key: "nonexistent")
              }.to raise_error(UNotifier::UnknownLocaleKeyError)
            end
          end
        end

        context "when there's no :locale_key in parameters" do
          it "raises EmptyLocaleKeyError" do
            expect { described_class.locale_key_for("second_category.n_multiple_locales") }
              .to raise_error(UNotifier::EmptyLocaleKeyError)
          end
        end
      end
    end

    context "when notification key doesn't exist" do
      it "raises NotificationNotFoundError" do
        expect { described_class.locale_key_for("nonexistent") }
          .to raise_error(UNotifier::NotificationNotFoundError)
      end
    end
  end

  describe "#notify" do
    before do
      allow(UNotifier).to receive(:notify_target)
    end

    let(:key) { "first_category.n_regular" }

    context "when multiple targets passed" do
      it "send notification to each target" do
        targets = [target, target]
        UNotifier.notify(key, targets)
        expect(UNotifier).to have_received(:notify_target).twice
      end
    end

    context "when one target passed" do
      it "send notification to one target" do
        UNotifier.notify(key, target)
        expect(UNotifier).to have_received(:notify_target).once
      end
    end
  end

  describe "#notify_target" do
    let(:key) { "first_category.n_regular" }
    let(:params) { Hash.new }
    let(:notification_entity) { double("Notification Entity") }
    let(:user_settings) { "external" }

    before do
      allow(I18n).to receive(:t).and_return("")
      allow(notification_entity).to receive(:key).and_return(key)
      allow(notification_entity).to receive(:save!)
      allow(notification_entity).to receive(:target).and_return(target)
      allow(notification_entity).to receive(:urgency).and_return(notifications_fixture.dig(*key.split("."), "user", "urgency"))
      allow(target).to receive(:locale).and_return(:en)
      allow(target).to receive(:notification_settings).and_return("user" => { key => user_settings })
      allow(target).to receive(:online?).and_return(true)
      allow(resource_class).to receive(:new).and_return(notification_entity)
    end

    subject { UNotifier.notify_target(key, target, params) }

    it "creates notification entity" do
      expect(notification_entity).to receive(:save!)
      expect(resource_class).to receive(:new).and_return(notification_entity)
      subject
    end

    it "calls user notifier" do
      expect(UNotifier::UserNotifier).to receive(:call).with(notification_entity, params)
      subject
    end

    it "calls system notifier" do
      expect(UNotifier::SystemNotifier).to receive(:call).with(notification_entity, params[:system])
      subject
    end

    context "when locale params provided" do
      let(:params) { { first: "first", second: "second" } }

      it "passes locale params to I18n" do
        locale_key = described_class.locale_key_for(key)
        expect(I18n).to receive(:t).with(/#{locale_key}.(body|title)/, hash_including(params))
        subject
      end
    end
  end

  describe "#notification_settings_for" do
    let(:user_settings) {
      {
        "user" => {
          "first_category.n_regular" => "onsite",
          "first_category.n_optional" => "off",
        },
        "options" => {
          "hide_sensitive" => true,
        },
      }
    }

    before do
      allow(target).to receive(:notification_settings).and_return(user_settings)
    end

    subject { UNotifier.notification_settings_for(target) }

    context "when user have setting value for the key" do
      it "loads value from user's settings" do
        expect(subject["user"]["optional"]).to include("first_category.n_optional" => "off")
        expect(subject["user"]["regular"]).to include("first_category.n_regular" => "onsite")
      end
    end

    context "when user doesn't have setting value for the key" do
      it "sets '#{UNotifier::Settings::DEFAULT_URGENCY}' value for the key" do
        expect(subject["user"]["regular"]).to include(
          "second_category.n_target_dependent" => UNotifier::Settings::DEFAULT_URGENCY
        )
      end
    end

    context "when user has values in 'options' section" do
      it "returns all values" do
        expect(subject["options"]).to eq("hide_sensitive" => true)
      end
    end
  end

  describe "#urgency_for" do
    context "when notification key exists" do
      context "when notification spec has only one urgency option" do
        it "returns urgency for provided notification key" do
          expect(
            described_class.urgency_for("first_category.n_regular")
          ).to eq(
            notifications_fixture["first_category"]["n_regular"]["user"]["urgency"]
          )
        end
      end

      context "when notification urgency depends on target" do
        context "when targe is defined in config" do
          it "return urgency based on target" do
            expect(
              described_class.urgency_for("second_category.n_target_dependent", target: "user")
            ).to eq(
              notifications_fixture["second_category"]["n_target_dependent"]["user"]["target"]["user"]["urgency"]
            )
            expect(
              described_class.urgency_for("second_category.n_target_dependent", target: "admin")
            ).to eq(
              notifications_fixture["second_category"]["n_target_dependent"]["user"]["target"]["admin"]["urgency"]
            )
          end
        end

        context "when target is not defined in config" do
          it "raises UnknownTargetError" do
            expect {
              described_class.urgency_for("second_category.n_target_dependent", target: "unknown")
            }.to raise_error(UNotifier::UnknownTargetError)
          end
        end
      end
    end

    context "when notification key doesn't exist" do
      it "raises NotificationNotFoundError" do
        expect { described_class.urgency_for("nonexistent") }
          .to raise_error(UNotifier::NotificationNotFoundError)
      end
    end
  end
end
